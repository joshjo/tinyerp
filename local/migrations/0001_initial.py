# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Company'
        db.create_table(u'local_company', (
            ('ruc', self.gf('django.db.models.fields.CharField')(max_length=11, primary_key=True)),
            ('razon_social', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100, blank=True)),
            ('telephone', self.gf('django.db.models.fields.CharField')(max_length=9, blank=True)),
        ))
        db.send_create_signal(u'local', ['Company'])

        # Adding model 'Series'
        db.create_table(u'local_series', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['local.Company'])),
            ('domicile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['domiciles.Domicile'], unique=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'local', ['Series'])

        # Adding model 'Numbering'
        db.create_table(u'local_numbering', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('document', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['local.Series'])),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'local', ['Numbering'])


    def backwards(self, orm):
        # Deleting model 'Company'
        db.delete_table(u'local_company')

        # Deleting model 'Series'
        db.delete_table(u'local_series')

        # Deleting model 'Numbering'
        db.delete_table(u'local_numbering')


    models = {
        u'domiciles.domicile': {
            'Meta': {'object_name': 'Domicile'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'department': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'district': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'local.company': {
            'Meta': {'object_name': 'Company'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'blank': 'True'}),
            'razon_social': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'primary_key': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '9', 'blank': 'True'})
        },
        u'local.numbering': {
            'Meta': {'object_name': 'Numbering'},
            'document': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['local.Series']"})
        },
        u'local.series': {
            'Meta': {'object_name': 'Series'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['local.Company']"}),
            'domicile': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['domiciles.Domicile']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['local']