from django.db import models
from decimal import Decimal
from datetime import date

# Create your models here.
class UnitMeasurement (models.Model):
	symbol = models.CharField (max_length = 3)
	name = models.CharField (max_length = 20)
	value_int = models.IntegerField ()
	def __unicode__ (self):
		return self.name

class Tag (models.Model):	
	tag = models.CharField (max_length = 100)

	def __unicode__(self):
		return self.tag

class Product (models.Model):	
	name = models.ForeignKey (Tag, related_name='name', null = True)
	description = models.ManyToManyField (Tag, null = True, related_name='description')

	def description_toString(self):
		try:
			tags = self.description.all()
		except:
			tags = []
		resp = ''		
		for tag in tags:
			resp += tag.tag.title() + ', '
		return resp

	def toString(self):		
		try:
			resp = self.name.tag.title() + ', '
		except:
			resp = ''
		resp += self.description_toString()
		return resp

	def __unicode__ (self):
		return self.toString()

	def set_name (self, name):
		t_name, b = Tag.objects.get_or_create(tag = name.strip().lower())
		self.name = t_name		

	def set_description (self, description):
		tags = [ tag.strip().lower() for tag in description.split(",")]
		for tag in tags:
			if len (tag) > 0:
				t, b = Tag.objects.get_or_create(tag = tag)
				self.description.add (t)

class ProductPrice (models.Model):
	price = models.DecimalField (max_digits = 10, decimal_places = 2)
	unit = models.ForeignKey (UnitMeasurement)
	product = models.ForeignKey (Product)
	quantity = models.DecimalField(max_digits=8, decimal_places=2, default=1)
	min_initial_payment = models.DecimalField (max_digits = 10, decimal_places = 2, blank = True, null = True)
	create_at = models.DateField(default=date.today, blank = True)