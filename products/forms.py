# -*- coding: utf-8 -*-
from django import forms
from models import *

class SearchForm(forms.ModelForm):
	class Meta:
		model = Name
		exclude = ("type_product",)
	name = forms.CharField(widget = forms.TextInput(attrs = {'id':'input_name', 'placeholder':'Nombre del Producto', 'class':'form-control'}))
	# type_product = forms.ChoiceField(widget=forms.Select(attrs = {'id':'select_type_product', 'class':'form-control'}), choices=TYPECHOICE)
	description = forms.CharField(widget = forms.TextInput(attrs = {'id':'input_description', 'placeholder':'Descripcion', 'class':'form-control'}))


class NewProductForm (forms.ModelForm):
	name = forms.CharField(widget = forms.TextInput(attrs = {'id':'input_name', 'placeholder':'Nombre del Producto', 'class':'form-control'}))
	type_product = forms.ChoiceField(widget=forms.Select(attrs = {'id':'select_type_product', 'class':'form-control'}), choices=TYPECHOICE)
	code = forms.CharField(widget = forms.TextInput(attrs = {'id':'input_code', 'placeholder':'Código del Producto', 'class':'form-control'}))
	class Meta:
		model = Name

	def clean(self):
		super (NewProductForm, self).clean()
		code = self.cleaned_data.get('code')
		if len(Product.objects.filter (code = code)) > 0:
			 raise forms.ValidationError("El Código ya se encuentra asignado para otro producto")
		return self.cleaned_data

class EditPriceForm (forms.ModelForm):
	class Meta:
		model = Price
		exclude = ("product",)

	unit = forms.ModelChoiceField(queryset=UnitMeasurement.objects.all(), empty_label=None, widget=forms.Select(attrs = {'id':'select_unit', 'class':'form-control'}))
	quantity = forms.DecimalField(widget = forms.TextInput(attrs = {'id':'input_price', 'placeholder':u'Cant.', 'class':'form-control'}))
	price = forms.DecimalField(widget = forms.TextInput(attrs = {'id':'input_price', 'placeholder':u'Precio', 'class':'form-control input-right'}))
	min_initial_payment = forms.DecimalField(widget = forms.TextInput(attrs = {'id':'ti_min_initial', 'placeholder':u'Acta. Min', 'class':'form-control input-right'}))

	def __init__ (self, product, is_new, *args, **kwargs):
		self.product = product
		self.is_new = is_new
		super (EditPriceForm, self).__init__(*args, **kwargs)

	def clean (self):
		super (EditPriceForm, self).clean()
		initial = self.cleaned_data.get('min_initial_payment')
		price = self.cleaned_data.get('price')
		if self.is_new:
			qty = self.cleaned_data.get('quantity')
			unit = self.cleaned_data.get('unit')
			instances = Price.objects.filter (quantity = qty, unit = unit, product = self.product)
			if len(instances) > 0:
				raise forms.ValidationError("Ya existe un registro con la misma cantidad y unidad")
		if initial > price:
			raise forms.ValidationError("El valor mínimo de A cuenta no puede ser mayor al importe Total")
		return self.cleaned_data

	def save(self, product_id, commit = True):
		instance = forms.ModelForm.save(self, False)
		instance.product = product_id
		instance.save()
		return instance