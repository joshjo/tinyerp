from django.conf.urls import patterns, url
from django.shortcuts import render
from clients import views
from clients.views import ClientListJSON

urlpatterns = patterns('',
	url(r'^$', views.all, name = "all"),
	url(r'^(?P<client_id>\d+)/$', views.detail, name = "detail"),
	url(r'^edit/(?P<client_id>\d+)/$', views.edit, name = "edit"),
	url(r'^_get_districts/$', views._get_districts, name = "getdistricts"),
	url(r'^_get_jsonlist/$', ClientListJSON.as_view(), name = "jsonlist"),
	url(r'^_get_provinces/$', views._get_provinces, name = "getprovinces"),
	url(r'^new/$', views.edit, name = "new"),
)