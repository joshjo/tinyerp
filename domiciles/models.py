#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
class Domicile (models.Model):
	address = models.CharField (max_length = 100)
	district = models.CharField (max_length = 25)
	province = models.CharField (max_length = 25)
	department = models.CharField (max_length = 25)

	def toString(self):
		return '%s - %s - %s' % (self.province, self.department, self.district)

	def __unicode__ (self):
		return '%s: %s: %s' % (self.province, self.department, self.district)