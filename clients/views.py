from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponseRedirect
from models import Client, Domicile
from forms import ClientForm
from domiciles.forms import DomicileForm
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.forms.models import model_to_dict
from django.db.models import Q
from django.core.urlresolvers import reverse
from helpers.ajax import ajax_view, AjaxError

dpd = {'AREQUIPA':{
		'AREQUIPA':['AREQUIPA', 'ALTO SELVA ALEGRE', 'CAYMA', 'JACOBO HUNTER', 'JOSE LUIS BUSTAMANTE Y RIVERO', 'MARIANO MELGAR', 'MIRAFLORES', 'PAUCARPATA','YANAHUARA'],
		'ISLAY':[ 'LA JOYA', 'MAJES', 'VITOR']
		}
	  }

@ajax_view
def _get_provinces (request):
	department = request.GET.get('department', 'AREQUIPA')
	try: provinces = dpd[department].keys()
	except: provinces = []
	return { 'provinces' : provinces }

@ajax_view
def _get_districts (request):
	department = request.GET.get('department', 'AREQUIPA')
	province = request.GET.get('province', 'AREQUIPA')
	try: districts = dpd[department][province]
	except: districts = []
	return { 'districts' : districts }

def all (request):
	client_list = ClientListJSON()
	client_list.request = request
	data = client_list.get_context_data()
	return render(request, 'clients/all.html', {'data': data})

def detail(request, client_id):
	client = get_object_or_404(Client, pk=client_id)
	return render(request, 'clients/detail.html', {'client':client})

# Create your views here.
def edit(request, client_id = ''):
	if client_id != '':
		client_instance = Client.objects.get(pk = client_id)
		domicile_instance = Domicile.objects.get(pk = client_instance.domicile_id)
		action = reverse('clients:edit', args=[client_id] )
	else:
		client_instance = None
		domicile_instance = None
		action = reverse('clients:new')
	if request.method == 'POST':
		department, province, district = request.POST.get('department'), request.POST.get('province'), request.POST.get('district')
		form_client = ClientForm (request.POST, instance = client_instance )
		form_domicile = DomicileForm (request.POST, instance = domicile_instance )
		if form_client.is_valid() and form_domicile.is_valid():
			d = form_domicile.save(commit = False)
			d.department, d.province, d.district = department, province, district
			d.save()
			client = form_client.save(commit = False)
			client.domicile = d
			client.save()
			return HttpResponseRedirect( reverse ('clients:detail', args =[client.ruc] ) )
	else:		
		form_client = ClientForm (instance = client_instance)
		form_domicile = DomicileForm (instance = domicile_instance)
	return render (request, 'clients/edit.html', {'form_client':form_client, 'form_domicile':form_domicile, 'action':action, 'departments':dpd.keys(), 'provinces':sorted(dpd['AREQUIPA'].keys()), 'districts':dpd['AREQUIPA']['AREQUIPA']})

class ClientListJSON(BaseDatatableView):
	model = Client
	columns = ['ruc', 'razon_social', 'telephone', 'email']
	order_columns = ['ruc', 'razon_social', 'telephone', 'email']
	max_display_length = 100
	def render_column(self, row, column):		
		if column == 'ruc':
			return '<a href="/clients/%s"> %s </a>' % (row.ruc, row.ruc)
		else:
			return super(ClientListJSON, self).render_column(row, column)

	def filter_queryset(self, qs):
		sSearch = self.request.GET.get('sSearch', None)
		if sSearch:
			qs = qs.filter(Q(razon_social__istartswith=sSearch) | Q(ruc__istartswith=sSearch))
		return qs
