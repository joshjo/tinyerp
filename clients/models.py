#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from domiciles.models import Domicile

# Create your models here.

class Client (models.Model):
	ruc = models.CharField (max_length = 11, primary_key = True)
	razon_social = models.CharField (max_length = 200)
	email = models.EmailField (max_length = 100, blank = True, null = True)
	telephone = models.CharField (max_length = 9, blank = True, null = True)
	domicile = models.OneToOneField (Domicile)

	def __unicode__ (self):
		return '%s: %s' % (self.ruc, self.razon_social)

	def to_dict (self):
		return {'ruc': self.ruc, 'razon_social': self.razon_social, 'domicile':self.domicile.toString() }