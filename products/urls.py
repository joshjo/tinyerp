from django.conf.urls import patterns, url
from django.shortcuts import render
from products import views

urlpatterns = patterns('',
	url(r'^$', views.search, name = "search"),
	url(r'^new/$', views.newproduct, name = "new"),
	url(r'^_getvalues/$', views._get_values, name = "ajaxvalues"),
	url(r'^temp/$', views.temp, name = "temp"),
	url(r'^_getattributes/$', views._get_attributes, name = "ajaxattributes"),
	url(r'^(?P<product_code>.+)/editprice/(?P<price_id>\d+)/$', views.editprice, name = "editprice"),
	url(r'^(?P<product_code>.+)/newprice/$', views.editprice, name = "newprice"),
	url(r'^(?P<product_code>.+)/$', views.detail, name = "detail"),
)