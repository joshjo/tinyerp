#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from models import Client

class ClientForm(forms.ModelForm):
	ruc = forms.CharField(widget = forms.TextInput(attrs = {'id':'input_ruc', 'placeholder':'RUC', 'maxlength':11, 'class':'form-control'}))
	razon_social = forms.CharField(widget = forms.TextInput(attrs = {'id':'input_razon_social', 'placeholder':u'Razón Social', 'class':'form-control'}))
	email = forms.EmailField(required=False, widget = forms.TextInput(attrs = {'id':'input_email', 'placeholder':'Email', 'class':'form-control'}))
	telephone = forms.CharField(required=False, widget = forms.TextInput(attrs = {'id':'input_telephone', 'placeholder':u'Teléfono', 'class':'form-control'}))
	class Meta:
		model = Client
		exclude = ('domicile')