# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from models import *
from forms import SearchForm, EditPriceForm, NewProductForm
from helpers.ajax import ajax_view, AjaxError, ajax_simple

@ajax_simple
def _get_values (request):
	attr = request.GET.get('attr', 'COPIAS')
	values = Attribute.objects.filter(attribute = attr)
	values = [i.value for i in values]
	return values

@ajax_simple
def temp(request):
	attr = request.GET.get('attr', 'COPIAS')
	values = Attribute.objects.filter(attribute = attr)
	values = [i.value for i in values]
	return values

def _attributes (product_type):
	if product_type == 'FORMATERIA':
		return ['COPIAS', 'COLORES', 'MATERIAL', 'TAMANO', 'NUMERACION']
	if product_type == 'SELLOS':
		return ['PORTASELLO']
	else: 
		return ['COLORES', 'MATERIAL', 'ACABADO', 'TAMANO', 'TROQUELADO', 'NUMERACION']

@ajax_simple
def _get_attributes (request):
	product_type = request.GET.get('product_type', 'FORMATERIA')
	return _attributes(product_type)

def _get_description (product):
	desc = ProductAttributes.objects.filter(product = product)
	tmp = ""
	for d in desc: tmp += "%s, " % (d.attribute.value)
	return tmp

def delete_price(request, product_id):	
	instance = get_object_or_404(ProductPrice, pk=request.GET.get('product_price_id', ''))
	instance.delete()
	return { 'res': 'ok', 'action':reverse ('products:detail', args=[product_id]) }

def detail (request, product_code):
	product = get_object_or_404(Product, code=product_code)
	description = _get_description(product)
	prices = Price.objects.filter(product = product)
	return render(request, 'products/detail.html', {'product':(product, description), 'prices': prices})

def editprice (request, product_code, price_id = ''):	
	product = get_object_or_404(Product, code=product_code)
	description = _get_description(product)
	if price_id != '':
		price_instance = Price.objects.get(pk = price_id)
		action = reverse('clients:edit', args=[price_id] )
	else:
		price_instance = None
		action = reverse('clients:new')
	if request.method == 'POST':
		form_price = EditPriceForm (data = request.POST, instance = price_instance, product = product, is_new = price_instance == None)
		if form_price.is_valid():
			form_price.save(product)
			return HttpResponseRedirect( reverse ('products:detail', args =[product_code] ) )
	else:
		form_price = EditPriceForm (instance = price_instance, product = product, is_new = True)
	return render (request, 'products/editprice.html', {'form_price':form_price, 'product':product, 'description':description})

def newproduct (request):
	if request.method == 'POST':
		form_product = NewProductForm(request.POST)
		if form_product.is_valid():
			type_product = dict(form_product.fields['type_product'].choices)[int(form_product.cleaned_data.get('type_product'))]
			attrs = _attributes(type_product)
			name, created = Name.objects.get_or_create(name = form_product.cleaned_data.get('name').upper(),type_product = form_product.cleaned_data.get('type_product'))
			product, created = Product.objects.get_or_create (name = name, code = form_product.cleaned_data.get('code').upper())
			for attr in attrs:
				attribute, created = Attribute.objects.get_or_create(attribute = attr.upper(), value = request.POST.get(attr).upper())
				ProductAttributes.objects.create(attribute = attribute, product = product)
			return HttpResponseRedirect( reverse ('products:search' ) )
	else:
		form_product = NewProductForm()
	return render(request, 'products/newproduct.html', {'form_product':form_product})

def search (request):
	name = ""
	description = ""
	type_product = -1
	context = {}
	if request.method == 'POST':
		name, description, type_product = request.POST.get('name'), request.POST.get('description'), request.POST.get('type_product')
		context['fieldsearch'] = name + " " + description	
	description = description.upper().split()
	limit = 20
	products = Product.objects.filter(name__name__icontains = name).order_by('-ranking')[:limit]
	data = []
	for product in products:
		desc = ProductAttributes.objects.filter(product = product)
		tmp = _get_description(product)
		append_data = True
		for term in description:
			if term not in tmp: append_data = False
		if append_data: data.append( (product, tmp) )
	form = SearchForm()
	context['data'] = data
	context['form'] = form
	return render(request, 'products/search.html', context)