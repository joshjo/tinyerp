{
'COPIAS':{'values':['SOLO ORIGINAL', 'ORIGINAL + 1 COPIA', 'ORIGINAL + 2 COPIAS', 'ORIGINAL + 3 COPIAS', 'ORIGINAL + 4 COPIAS', 'ORIGINAL + 5 COPIAS'], 'printorder':'v'},
'COLORES':{'values':['1','2','3','4','5'], 'printorder':'va'},
'PAPEL':{'values':['AUTOCOPIATIVO', 'NORMAL'], 'printorder':'av'},
'TAMAÑO':{'values':['HOJA ENTERA', '1/2 OFICIO', '1/4 OFICIO', '1/6 OFICIO', '1/8 OFICIO', '1/3 OFICIO'],'printorder':'v'},
'ACABADO':{'values':['BARNIZADO', 'BARNIZADO UV', 'PLASTIFICADO BRILLO', 'PLASTIFICADO MATE', 'TROQUELADO', 'NUMERADO'],'printorder':'v'},
'MATERIAL':{'values':['PAPEL BOND', 'FOLDCOTE', 'COUCHE', 'CARTULINA KIMBERLY', 'CARTULINA ESCOLAR'], 'printorder':'v'},
}