#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from domiciles.models import Domicile

class DomicileForm(forms.ModelForm):
	address = forms.CharField(widget = forms.TextInput(attrs = {'id':'input_address', 'class': 'form-control', 'placeholder':'Calle, Avenida'}))
	class Meta:
		model = Domicile