# -*- coding: utf-8 -*-
from django.db import models
from domiciles.models import Domicile

# Create your models here.
class Company (models.Model):
	ruc = models.CharField (max_length = 11, primary_key = True)
	razon_social = models.CharField (max_length = 200)
	email = models.EmailField (max_length = 100, blank = True)
	telephone = models.CharField (max_length = 9, blank = True)	

class Series (models.Model):
	company = models.ForeignKey (Company)
	domicile = models.OneToOneField (Domicile)
	number = models.IntegerField ()

DOCUMENTCHOICE = (
	('bv', 'Boleta de Venta'),
	('fa', 'Factura'),
	('ot', 'Orden de Trabajo'),
	('gr', 'Guia de Remisión'),
	('nc', 'Nota de Credito'),
	('nd', 'Nota de Debito'),
	('lc', 'Liquidación de Compra'),
)

class Numbering (models.Model):
	document = models.CharField(max_length=2, choices=DOCUMENTCHOICE)
	series = models.ForeignKey (Series)
	number = models.IntegerField ()