import os
import sys
from fabric.api import *
from fabric.colors import green, red
from fabric.contrib import django
django.settings_module('settings')
from django.conf import settings

def migrationall():
    APPS_TO_WATCH = ['clients','documents','domiciles','products']
    for app in APPS_TO_WATCH:
        local('python manage.py schemamigration %s --auto' % app)