# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Client'
        db.create_table(u'clients_client', (
            ('ruc', self.gf('django.db.models.fields.CharField')(max_length=11, primary_key=True)),
            ('razon_social', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=100, null=True, blank=True)),
            ('telephone', self.gf('django.db.models.fields.CharField')(max_length=9, null=True, blank=True)),
            ('domicile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['domiciles.Domicile'], unique=True)),
        ))
        db.send_create_signal(u'clients', ['Client'])


    def backwards(self, orm):
        # Deleting model 'Client'
        db.delete_table(u'clients_client')


    models = {
        u'clients.client': {
            'Meta': {'object_name': 'Client'},
            'domicile': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['domiciles.Domicile']", 'unique': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'razon_social': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'primary_key': 'True'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '9', 'null': 'True', 'blank': 'True'})
        },
        u'domiciles.domicile': {
            'Meta': {'object_name': 'Domicile'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'department': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'district': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        }
    }

    complete_apps = ['clients']