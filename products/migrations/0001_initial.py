# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UnitMeasurement'
        db.create_table(u'products_unitmeasurement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('symbol', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('value_int', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'products', ['UnitMeasurement'])

        # Adding model 'Attribute'
        db.create_table(u'products_attribute', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('attribute', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'products', ['Attribute'])

        # Adding unique constraint on 'Attribute', fields ['attribute', 'value']
        db.create_unique(u'products_attribute', ['attribute', 'value'])

        # Adding model 'Name'
        db.create_table(u'products_name', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('type_product', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'products', ['Name'])

        # Adding model 'Product'
        db.create_table(u'products_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.Name'])),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=10)),
            ('ranking', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True)),
        ))
        db.send_create_signal(u'products', ['Product'])

        # Adding model 'ProductAttributes'
        db.create_table(u'products_productattributes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.Product'])),
            ('attribute', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.Attribute'])),
        ))
        db.send_create_signal(u'products', ['ProductAttributes'])

        # Adding unique constraint on 'ProductAttributes', fields ['product', 'attribute']
        db.create_unique(u'products_productattributes', ['product_id', 'attribute_id'])

        # Adding model 'Price'
        db.create_table(u'products_price', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('unit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.UnitMeasurement'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.Product'])),
            ('quantity', self.gf('django.db.models.fields.DecimalField')(default=1, max_digits=8, decimal_places=2)),
            ('min_initial_payment', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'products', ['Price'])


    def backwards(self, orm):
        # Removing unique constraint on 'ProductAttributes', fields ['product', 'attribute']
        db.delete_unique(u'products_productattributes', ['product_id', 'attribute_id'])

        # Removing unique constraint on 'Attribute', fields ['attribute', 'value']
        db.delete_unique(u'products_attribute', ['attribute', 'value'])

        # Deleting model 'UnitMeasurement'
        db.delete_table(u'products_unitmeasurement')

        # Deleting model 'Attribute'
        db.delete_table(u'products_attribute')

        # Deleting model 'Name'
        db.delete_table(u'products_name')

        # Deleting model 'Product'
        db.delete_table(u'products_product')

        # Deleting model 'ProductAttributes'
        db.delete_table(u'products_productattributes')

        # Deleting model 'Price'
        db.delete_table(u'products_price')


    models = {
        u'products.attribute': {
            'Meta': {'unique_together': "(('attribute', 'value'),)", 'object_name': 'Attribute'},
            'attribute': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'products.name': {
            'Meta': {'object_name': 'Name'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type_product': ('django.db.models.fields.IntegerField', [], {})
        },
        u'products.price': {
            'Meta': {'object_name': 'Price'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'min_initial_payment': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Product']"}),
            'quantity': ('django.db.models.fields.DecimalField', [], {'default': '1', 'max_digits': '8', 'decimal_places': '2'}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.UnitMeasurement']"})
        },
        u'products.product': {
            'Meta': {'object_name': 'Product'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Name']"}),
            'ranking': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'})
        },
        u'products.productattributes': {
            'Meta': {'unique_together': "(('product', 'attribute'),)", 'object_name': 'ProductAttributes'},
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Attribute']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Product']"})
        },
        u'products.unitmeasurement': {
            'Meta': {'object_name': 'UnitMeasurement'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'value_int': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['products']