from django.db import models
from decimal import Decimal
from datetime import date

# Create your models here.

class UnitMeasurement (models.Model):
	symbol = models.CharField (max_length = 3)
	name = models.CharField (max_length = 20)
	value_int = models.IntegerField ()
	def __unicode__ (self):
		return self.name

class Attribute (models.Model):	
	attribute = models.CharField (max_length = 30)
	value = models.CharField (max_length = 30)
	class Meta:
		unique_together = ('attribute', 'value',)

	def to_dict (self):
		return {self.attribute: self.value}

TYPECHOICE = (
	(0, 'FORMATERIA'),
	(1, 'SELLOS'),
	(2, 'OTROS'),
)

class Name (models.Model):
	name = models.CharField (max_length = 50)
	type_product = models.IntegerField(choices=TYPECHOICE)

class Product (models.Model):
	name = models.ForeignKey (Name)
	code = models.CharField (max_length=10, unique=True)
	ranking = models.IntegerField (db_index=True, default=0)

class ProductAttributes (models.Model):
	product = models.ForeignKey (Product)
	attribute = models.ForeignKey (Attribute)
	class Meta:
		unique_together = ('product', 'attribute',)

class Price (models.Model):
	price = models.DecimalField (max_digits = 10, decimal_places = 2)
	unit = models.ForeignKey (UnitMeasurement)
	product = models.ForeignKey (Product)
	quantity = models.DecimalField(max_digits=8, decimal_places=2, default=1)
	min_initial_payment = models.DecimalField (max_digits = 10, decimal_places = 2, blank = True, null = True)