# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Domicile'
        db.create_table(u'domiciles_domicile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('district', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('province', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('department', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal(u'domiciles', ['Domicile'])


    def backwards(self, orm):
        # Deleting model 'Domicile'
        db.delete_table(u'domiciles_domicile')


    models = {
        u'domiciles.domicile': {
            'Meta': {'object_name': 'Domicile'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'department': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'district': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        }
    }

    complete_apps = ['domiciles']